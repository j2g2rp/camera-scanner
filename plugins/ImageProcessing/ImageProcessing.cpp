#include <QDebug>
#include <QString>
#include <QtGui/QImage>

#include <cmath>
#include <iostream>
#include <vector>

#include <QDir>
#include <QFileInfo>

#include <libintl.h>
#define _(value) gettext(value)

#include "ImageProcessing.h"

using namespace DocumentScanner;

const QString GETTEXT_DOMAIN = "camerascanner.jonnius";

ImageProcessing::ImageProcessing()
    : m_store(*DocumentStore::instance())
{
    textdomain(GETTEXT_DOMAIN.toStdString().c_str());
    /* empty */
    // TODO define default params
    // TODO load params from config
}

void ImageProcessing::restoreCache()
{
    bool any = false;
    for (QString id : m_store.restoreCache()) {
        emit imageAdded(id);
        any = true;
    }

    if (any)
        emit userInfo(_("Session restored"));

    validateIsAnyDoc();
}

void ImageProcessing::addImage(const QString &imageURL)
{
    QString id = m_store.addDocument(imageURL);
    m_store.cacheDocument(id);
    emit imageAdded(id);
    validateIsAnyDoc();
}

void ImageProcessing::removeImage(const QString &id)
{
    m_store.removeDocument(id);
    emit imageRemoved(id);
    validateIsAnyDoc();
}

void ImageProcessing::removeAll()
{
    for (QString id : m_store.getIDs())
        removeImage(id);
    validateIsAnyDoc();
}

QString ImageProcessing::exportAsPdf(const QString &id) const
{
    QStringList ids = { id };
    return m_store.exportPdf(ids);
}

QString ImageProcessing::exportAllAsPdf() const
{
    return m_store.exportPdf(m_store.getIDs());
}

QString ImageProcessing::exportAsImage(const QString &id) const
{
    return m_store.getImageURL(id);
}

QStringList ImageProcessing::exportAllAsImages() const
{
    QStringList ids = m_store.getIDs();
    QStringList urls;
    for (QString id : ids) {
        urls << m_store.getImageURL(id);
    }
    return urls;
}

bool ImageProcessing::isDocument(const QString &id) const
{
    return m_store.isExtractedDoc(id);
}

bool ImageProcessing::isAnyDocument() const
{
    return m_isAnyDocument;
}

void ImageProcessing::validateIsAnyDoc()
{
    bool isAnyDocument = false;
    QStringList ids = m_store.getIDs();
    for (QString id : ids) {
        if (isDocument(id)) {
            isAnyDocument = true;
        }
    }

    if (isAnyDocument != m_isAnyDocument) {
        m_isAnyDocument = isAnyDocument;
        emit isAnyDocChanged();
    }
}
