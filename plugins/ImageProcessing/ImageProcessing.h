#ifndef IMAGEPROCESSING_H
#define IMAGEPROCESSING_H

#include <QImage>
#include <QObject>

#include "DocumentStore.h"

/**
 * Qt Object that acts as an interface to the Image Processing.
 */
class ImageProcessing : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool isAnyDocument READ isAnyDocument NOTIFY isAnyDocChanged)

public:
    ImageProcessing();
    ~ImageProcessing() = default;

    /** Load cached images from disk and add them */
    Q_INVOKABLE void restoreCache();
    /** Add an image and cache it on disk */
    Q_INVOKABLE void addImage(const QString &imageURL);
    /** Remove the specified image and delete it from cache */
    Q_INVOKABLE void removeImage(const QString &id);
    /** Remove all images and clear the cache */
    Q_INVOKABLE void removeAll();
    /** Export one image as PDF and return URL to PDF file */
    Q_INVOKABLE QString exportAsPdf(const QString &id) const;
    /** Export all images as PDF and return URL to PDF file */
    Q_INVOKABLE QString exportAllAsPdf() const;
    /** Export one image and return URL to image file */
    Q_INVOKABLE QString exportAsImage(const QString &id) const;
    /** Export all images and return list of URLs to image files */
    Q_INVOKABLE QStringList exportAllAsImages() const;

    /** Return true if a document has been found in the specified image */
    Q_INVOKABLE bool isDocument(const QString &id) const;
    /** Return true if a document has been found in the current session */
    bool isAnyDocument() const;

private:
    void validateIsAnyDoc();

signals:
    void isAnyDocChanged();
    void imageAdded(const QString &id);
    void imageRemoved(const QString &id);
    void userInfo(const QString msg);

private:
    DocumentScanner::DocumentStore &m_store;
    std::map<QString, QVariant> m_params;
    bool m_isAnyDocument = false;
};

#endif
